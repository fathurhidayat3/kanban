export const populateListItems = function(listData, listGroupId) {
  const kbCardListGroup = document.getElementById(listGroupId);

  listData.map(listDataItem => {
    let listItemNode = document.createTextNode(listDataItem);
    let kbCardListItem = document.createElement("li");

    kbCardListItem.setAttribute("class", "kb-card__list-item");

    kbCardListItem.appendChild(listItemNode);
    kbCardListGroup && kbCardListGroup.appendChild(kbCardListItem);
  });
};
