export const sortData = (data, dir = "asc") => {
  let temp = data.slice().sort();

  return dir === "desc" ? temp.reverse() : temp;
};
