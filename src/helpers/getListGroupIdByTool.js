export const getListGroupIdByTool = toolItem => {
  return toolItem.parentElement.parentElement.nextElementSibling.children[0].id;
};
