// @flow

import { getListGroupIdByTool, sortData, populateListItems } from "./helpers";

let todoData = [
  "Deploy Project Ecommerce",
  "Restyling Navbar Component",
  "Fix User Login Flow"
];
let inProgressData = ["Dockerize Ecommerce", "Test Sidebar Components"];
let reviewData = [];
let doneData = ["Resolve Conflict Branch develop On Mobile"];

// Init
const cardSelectors = ["kb-card-1", "kb-card-2", "kb-card-3", "kb-card-4"];
const cardDatas = [todoData, inProgressData, reviewData, doneData];

cardSelectors.map((cardSelectorItem, index) =>
  populateListItems(cardDatas[index], cardSelectorItem)
);

// Features
let kbAddItem = document.querySelectorAll(".kb-tool--add-item");
kbAddItem.forEach(addItemItem => {
  addItemItem.addEventListener("click", () => {});
});

let kbSortAsc = document.querySelectorAll(".kb-tool--asc");
kbSortAsc.forEach(sortAscItem => {
  sortAscItem.addEventListener("click", () => {
    let cardData = sortData(todoData);
    let kbCardListGroupId = getListGroupIdByTool(sortAscItem);

    let myNode = document.getElementById(kbCardListGroupId);
    while (myNode && myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }

    populateListItems(cardData, kbCardListGroupId);
  });
});

let kbSortDesc = document.querySelectorAll(".kb-tool--desc");
kbSortDesc.forEach(sortDescItem => {
  sortDescItem.addEventListener("click", () => {
    let cardData = sortData(todoData, "desc");
    let kbCardListGroupId = getListGroupIdByTool(sortDescItem);

    let myNode = document.getElementById(kbCardListGroupId);
    while (myNode && myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }

    populateListItems(cardData, kbCardListGroupId);
  });
});

let kbSortReset = document.querySelectorAll(".kb-tool--reset");
kbSortReset.forEach(sortResetItem => {
  sortResetItem.addEventListener("click", () => {
    let cardData = todoData;
    let kbCardListGroupId = getListGroupIdByTool(sortResetItem);

    let myNode = document.getElementById(kbCardListGroupId);
    while (myNode && myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }

    populateListItems(cardData, kbCardListGroupId);
  });
});
